# ForYouAll 1.20.1
A welcoming modpack designed for that cozy feeling with ya less tech-savy friends. Packs pretty a bit of everything, with focus on technology & exploration.

Try avoiding installing mods from multiple sources (Modrinth/Curseforge/Custom). Since ones from other platformsmay get packed into the finall installation file, thus possibly increasing its size.

## Installation
Download the latest release: <a href="https://gitlab.com/HiWhatName/AllForYouModpack/releases/">here</a>

## Addings mods:
Important: Try avoiding installing mods from multiple sources (Modrinth/Curseforge/Custom). Since ones from other platforms than the main one (Curseforge) will be packed into the finall installation file, thus increasing its size.


`packwiz curseforge install URL`, will install a mod from the curseforge repositories and it's dependencies. Tho, some are locked behind their api and must be installed manually.

`packwiz modrinth install URL/ID`, same as above but should work all the time.

If you added any mods yourself, be sure to run `packwiz refresh`.

## How to export (Client and Server)
[NOTE] this repository doesn't contain the actual jar files used to start the modpack, they can be packed into a modrinth file and extracted from there.

#### Curseforge
`packwiz curseforge export --side [server/client/both]`
This will create a .zip file, which can be read by the offical launcher to download the mods.

#### Packwiz
`packwiz modrinth export --side [server/client/both]`
Same as above but creates a way larger file containing all the mods already packed into the file itself. *(May be used by MultiMC to create a .mm instance)*


#### Mod recommendations:
- You may add DeathBan next to Limited lives for a short ban on death.
- Dnymapforge can be used for serving an online map.


### Licensing
While this project is provided under the GPL-3.0 license, the following submodules have their own:
- Sildur's Vibrant shaders v1.51 located at: https://sildurs-shaders.github.io/downloads/
  and at their CF page: https://www.curseforge.com/minecraft/shaders/sildurs-vibrant-shaders/files/4688977
- Default-Dark-Mode located at https://github.com/nebuIr/Default-Dark-Mode/releases/tag/v1.3.4
- Super-Duper-Vanila located at https://github.com/Eldeston/Super-Duper-Vanilla/releases/tag/v1.3.0-beta.3

(Yes, this README has been copied from another project.)

