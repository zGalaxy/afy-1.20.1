// Inspired by previous modpacks.
WorldgenEvents.remove(event => {
  event.removeOres(props => {
    props.worldgenLayer = 'underground_ores';
    props.blocks = [
      // All the ores already adds zinc to the game.
      "create:zinc_ore",
      "create:deepslate_zinc_ore",

      // Replaced by ??
      "bigreactors:yellorite_ore"  
    ];
  });
});

